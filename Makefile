flags = felf64
linker = ld
compiler = nasm
.PHONY: clean

program: main.o lib.o dict.o
	$(linker) -o $@ $^

main.o: main.asm
	$(compiler) -$(flags) -o $@ $<

lib.o: lib.asm
	$(compiler) -$(flags) -o $@ $<

dict.o: dict.asm
	$(compiler) -$(flags) -o $@ $<

clean:
	rm *.o program