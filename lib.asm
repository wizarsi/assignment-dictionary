section .text

global exit
global string_length
global string_equals
global print_string
global print_newline
global print_uint
global print_int 
global print_char
global print_uint
global print_int
global parse_uint
global parse_int
global read_char
global read_word
global print_error




; Принимает указатель на нуль-терминированную строку, выводит её в sterr
print_error:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdi, 2
    mov rdx, rax
    mov rax, 1
    syscall

    ret

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .count:
      cmp byte [rdi], 0  ;проверка на нуль-терминатор
      je .end
      inc rax
      inc rdi
      jmp .count
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdi, 1
    mov rdx, rax
    mov rax, 1
    syscall

    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    mov rsi, rsp ; здесь обязательно нужно передать укзатель
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_string

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12 ; calle-saved, регистр будет модифицирован
    mov r11, rsp
    dec rsp
    mov byte[rsp], 0 ;нуль-терминатор
    mov rax, rdi
    mov r12,10 ;делим на 10 для перевода в 10-ю систему счисления

    .divide_loop:
        xor rdx, rdx ; регистр, в котором храниться остаток от деления, он может переполнится
                     ; также обнудение важно для корректности результата
        div r12
        add rdx, "0" ;остаток деления пребавляем к коду символа 0, получаем код цифры в ASCII от 0(30) до 9(39)
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        je .main
        jmp .divide_loop
   .main:
        mov rdi, rsp
        push r11
        call print_string ; по порядку выводим выводим все цифры
        pop rsp

    .done:
        pop r12 ; calle restored register
        ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jns .end ; если число положительное сразу выводим его
    .print_minus:
        push rdi
        mov rdi, '-'
        call print_char ; выводим минус
        pop rdi
        neg rdi ;делаем число положительным
    .end:
        push rdi
        call print_uint ;выводим положительное число
        pop rdi
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    call string_length ; получаем длину строки
    pop rdi
    mov r11, rax
    xor rax,rax

    .compare_char:
        mov al, byte [rsi] ; указатель на текущий символ
        cmp al, byte [rdi] ; указатель на текущий символ
        jne .not_equals
        inc rsi ; получаем указатель на следующий символ
        inc rdi ; получаем указатель на следующий символ
        cmp r11,0 ; если прошлись по всем символам заканчиваем проверку
        je  .its_end ; пока не прошли всю строку производим сравнение
        dec r11
        jmp .compare_char
    .its_end:
        cmp al, 0 ; проверка последнего символа на нуль-терминатор
        jne .not_equals
        xor rax, 1 ; код возврата успех
        ret
    .not_equals:
        xor rax, rax ; код возврата неудача
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax

    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12 ;calle-saved registers дальше они будут изменяться
    push r13
    push r14
    
    mov r12, rsi
    mov r13, rdi
    .let_space_symbols: ;пропускаем пробельные символы
        call read_char
        cmp al, 0x20
        je .let_space_symbols
        cmp al, 0x9
        je .let_space_symbols
        cmp al, 10
        je .let_space_symbols
    xor r14, r14
    .read:
        cmp r12, r14 ; в r12 размер буфера, а r14 это счетчик, читаем слово пока r14<r12
        je .else  ;если привысим буфер, неудача
        mov byte[r13+r14], al ; записываем символ в буфер
        .is_it_end: ;если есть пробельные символы то слово закончилось
            cmp al, 0x20
            je .point_end
            cmp al, 0x9
            je .point_end
            cmp al, 10
            je .point_end
            cmp al, 0
            je .point_end
            inc r14
        call read_char ;не принимает параметров
        jmp .read
    .point_end:
        mov byte[r13+r14], 0 ;добовляем нуль-терминатор
        mov rax, r13 ;адрес буфера
        mov rdx, r14 ;длина слова
        jmp .end
    .else:
        xor rax, rax
    .end:
        pop r14 ;calle restored registers
        pop r13
        pop r12
        ret
      

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push r12 ; calle-saved регистры, будут модифицированы
    push r13
    .check_start:   ; проверка на цифру в начале строки
        mov al, byte[rdi]
        cmp al, "0"
        jb .else
        cmp al, "9"
        ja .else
    xor rax,rax
    push rdi ; сохраняем указатель на строку, дальше будет нужно для подсчета длины
    mov r12,10 ; дальше умножаем на 10, тем самым увеличиваю разрядность
               ; нужно для получения числового значения
    .parse_num:
        xor r13,r13
        .is_it_digit: ; проверка каждого символа на число
            mov r13b, byte[rdi]
            cmp r13b, "0"
            jb .point_end
            cmp r13b, "9"
            ja .point_end
            inc rdi
        .change_number: ; формирование числового значения
            mul r12     ; в аккумуляторе увеличиваем разрядность числа
            sub r13b, "0"
            add rax, r13 ; прибавляем цифру, которая должна быть в текущем разрядя
                         ; такой алгоритм работает, потому что мы читаем строку с числом,
                         ; начиная со старших разрядов
        jmp .parse_num
    .else:
        xor rdx,rdx
        jmp .end
    .point_end:
       pop rdx ;достаем указатель на строку
       sub rdx,rdi ; получаем длину числа в символах
       neg rdx  ; делаем значение положительным
    .end:
        pop r13 ;calle restored registers
        pop r12
        ret

        
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    .check_number_sign:
        mov al, byte[rdi]
        cmp al, '-'            ; провекрка на знак минус
        jne .parse_plus_number
        inc rdi                ; если число отрицательное передвигаем указатель только на число
    .is_it_digit: ;проверка на число
        mov al, byte[rdi]
        cmp al, '0'
        jl .else
        cmp al, '9'
        jg .else
    push rdi
    call parse_uint ;получаем числовое значение
    pop rdi
    cmp rdx,0
    je .else
    neg rax ; меням знак полученного безнакового числа
    inc rdx ; в rdx пришла длина безнакового числа, делаем инкремент, потому что в нашем случае есть "-"
    ret
    .parse_plus_number:
        push rdi ;caller-save register
        call parse_uint
        pop rdi ;caller restore register
        ret   
    .else:
        xor rax, rax
        xor rdx, rdx
        ret  

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push r12 ; calle-saved, регистр будет модифицирован
    push rdi ; caller-save
    call string_length
    pop rdi ; caller restore register

    cmp rdx, rax ; проверяем умещается ли наша строка в буфер
    jbe .else

    .cycle:
        xor r12, r12
        mov r12b, byte[rdi] ; копируем символ и строки в регистр
        mov byte[rsi], r12b ; копируем символ в буфер
        cmp r12, 0  ;  проверка на нуль-терминатор
        je .end
        inc rdi     ; следующий символ
        inc rsi
        jmp .cycle
    .else:
        xor rax, rax
    .end:
        pop r12 ;calle restored register
        ret
