extern string_equals
extern print_string

global find_word

section .text

%define reserved_bytes 8



; find_word пройдёт по всему словарю в поисках подходящего ключа.
; return: rax - адрес начала вхождения в словарь (не значения), иначе вернёт 0.
;
; Parametrs:
; rdi - Указатель на нуль-терминированную строку.
; rsi - Указатель на начало словаря.

find_word:
	.loop:
	
		add rsi, reserved_bytes	; 8 - это кол-во байт резервируемое dq
					; прибавляем к началу 8, чтобы получить адрес ключа
					; данные в памяти будут распаложены последовательно в независимости от db,dq... - у них нет отдельных блоков в ней
					
		push rsi 
		push rdi
		call string_equals
		pop rdi
		pop rsi

		cmp rax, 0
		jne .success
		mov rsi, [rsi - reserved_bytes] ; получаем адрес предыдыщего элемента, проходим по списку от его конца
		cmp rsi, 0 
		jne .loop ; если элемент последний адрес следующего элемента для него будет не инициализирован
	.fail:
		xor rax, rax
		ret
	.success:
		sub rsi, reserved_bytes
		mov rax, rsi 
		ret