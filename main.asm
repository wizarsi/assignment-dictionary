%include "colon.inc"
%include "words.inc"

extern print_string             
extern exit
extern find_word
extern read_word
extern print_error
extern print_newline

global _start

section .rodata
start: db "Введите ключ:", 10, 0
success_msg: db "Значение под таким ключем соответствует: ", 0
not_on_the_list: db "Такого ключа нет", 10, 0
buffer_overflow: db "Ключ должен быть короче 256 символов!", 10, 0
new_line: db 0xA,0

%define buffer_size 256
%define reserved_bytes 8

section .text

_start:
	sub rsp, buffer_size ; чтобы стек не съел программу нужно задать границы, в которых указатель стека может находиться
	mov rdi, start 
	call print_string

	mov rdi, rsp	; здесь происходит ввод ключа в буффер
	mov rsi, buffer_size
	call read_word
	cmp rax, 0
	je .overflow ; вышли за размеры буффера

	mov rdi, rax ; здесь мы проверяем есть ли ключ в списке
	mov rsi, addr
	push rdx ; сохраняем длину имени, дальше будем искать значение элемента
	call find_word
	pop rdx
	cmp rax, 0
	je .fail

.success:	
	add rax, reserved_bytes
	add rax, rdx ; 
	inc rax ; нуль-терминатор, поэтому плюс 1

	push rax
	mov rdi, success_msg
	call print_string ; сообщение об успехе
	pop rdi

	call print_string ; выводим значение

	mov rdi, new_line
	call print_string

	add rsp, buffer_size ; cбрасываем стек в исходное состояние
	call exit
.overflow:
	mov rdi, buffer_overflow
	jmp .error
.fail:
	mov rdi, not_on_the_list
.error:
	add rsp, buffer_size ; cбрасываем стек в исходное состояние
	call print_error	; в этой функции в rdi помещен дескриптор потока ошибок
	call exit
